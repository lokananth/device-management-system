<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Issue extends CI_Controller {	
	 
	function __Construct(){
		parent::__Construct ();
		session_start();	
		if($_SESSION['username']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$data = array();
		/*$varOperatorId = trim($_SESSION['operatorId']);
		$params = array('Operatorid'=>$varOperatorId);
		$arrActivationRequestRes = ApiPostHeader($this->config->item('GetActivationRequest'), $params);
		if(count($arrActivationRequestRes)>0 && $arrActivationRequestRes[0]['errcode']=='0'){			
				$data['arrActivationRequest'] = $arrActivationRequestRes;		
		}else{
			$data['arrActivationRequest'] = array();
		}			*/
		//echo '<pre>';print_r($_SESSION);print_r($params);print_r($arrActivationRequestRes);exit;
		
		$this->load->view('header_view');
		$this->load->view('innerMenu_view');
		//$this->load->view('leftMenu_view');
		$this->load->view('issueTracking_view',$data);
		$this->load->view('footer_view');
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */