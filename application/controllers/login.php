<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index()
	{
		//echo 'hai';
		$this->load->library('session');
		$this->load->view('header_view');
		$this->load->view('login_view');
		$this->load->view('footer_view');
	}
		
	public function checkUserLogin(){
		//echo 'hai';exit;
		//echo '<pre>';print_r($_REQUEST);exit;
		$this->load->library('session');
		
		$varUsername = trim($_REQUEST['username']);
		$varPassword = trim($_REQUEST['password']);
		
		$params = array('username'=>$varUsername,'Password'=>$varPassword);
		//$arrLoginInfo = ApiPostHeader($this->config->item('UserLogin'), $params);
		//echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;		
		
		//if($arrLoginInfo['errcode']==''){
			// log the ip address and last login user details
			//$params = array('user_id'=>$arrLoginInfo['Id'],'OperatorId'=>$arrLoginInfo['operator_id'],'ipaddress'=>$this->getClientIp(),'last_login'=>'2016-01-01');
			//$arrLoginIpLog = ApiPostHeader($this->config->item('GetUserLoginIP'), $params);	
			//echo '<pre>';print_r($params);print_r($arrLoginIpLog);exit;
			session_start();
			$_SESSION['username'] = $varUsername;
			$_SESSION['userId'] = $arrLoginInfo['Id'];
			$_SESSION['operatorId'] = $arrLoginInfo['operator_id'];
			$_SESSION['operatorName'] = $arrLoginInfo['OperatorName'];
			redirect('device');			
		//}else{
			//$this->session->set_flashdata('errmsg','Please enter a valid username & password');
			//redirect('login');
		//}		
	}
	
	public function getClientIp(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	public function UsersignOut(){
		session_start();
		session_destroy();
		session_unset();
		unset($_SESSION['username']);
		redirect('login');
	}		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */