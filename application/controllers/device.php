<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Device extends CI_Controller {	
	 
	function __Construct(){
		parent::__Construct ();
		session_start();	
		if($_SESSION['username']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{		
		$data = array();
		/*$varOperatorId = trim($_SESSION['operatorId']);
		$params = array('Operatorid'=>$varOperatorId);
		$arrActivationRequestRes = ApiPostHeader($this->config->item('GetActivationRequest'), $params);
		if(count($arrActivationRequestRes)>0 && $arrActivationRequestRes[0]['errcode']=='0'){			
				$data['arrActivationRequest'] = $arrActivationRequestRes;		
		}else{
			$data['arrActivationRequest'] = array();
		}			*/
		//echo '<pre>';print_r($_SESSION);print_r($params);print_r($arrActivationRequestRes);exit;
		
		$data['arrCountryList'] = ApiPostHeader($this->config->item('GetCountryDetails'), '');
		
		
		$this->load->view('header_view');
		$this->load->view('innerMenu_view');
		//$this->load->view('leftMenu_view');
		$this->load->view('device_view',$data);
		$this->load->view('footer_view');
	}
	
	public function deviceSearch(){
		//echo '<pre>';print_r($_REQUEST);exit;		
		$data['varDeviceStatus'] = trim($_REQUEST['deviceStatus']);		
		$arrCountry = explode('~',trim($_REQUEST['country']));		
		$data['varCountry'] = $arrCountry[1];
		
		$data['arrCountryList'] = ApiPostHeader($this->config->item('GetCountryDetails'), '');
		$this->load->view('header_view');
		$this->load->view('innerMenu_view');
		//$this->load->view('leftMenu_view');
		$this->load->view('device_view',$data);
		$this->load->view('footer_view');
	}
	
	public function addDeviceInformation(){
		echo '<pre>';print_r($_REQUEST);exit;
	}
	
	public function editDeviceInformation(){
		echo '<pre>';print_r($_REQUEST);exit;
	}
	
	public function retireDevice(){
		echo '<pre>';print_r($_REQUEST);exit;
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */