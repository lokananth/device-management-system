<aside id="left-panel">
	<!-- User info -->
	<div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 			
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
			<!--<i class="fa fa-lg fa-fw fa-user"></i>
				<img src="img/avatars/sunny.png" alt="me" class="online" /> -->
				<span></span> 
			</a> 			
		</span>
	</div>
	<!-- end user info -->

	<!-- NAVIGATION : This navigation is also responsive-->
	<nav>
		<ul>
			<li class="active">
				<a href="<?php echo base_url();?>device" title="Device List"><!-- fa-home --><i class="fa fa-lg fa-fw "></i> <span class="menu-item-parent">Device List</span></a>
			</li>
			<li>
				<a href="<?php echo base_url();?>issue" title="Issue Tracking"><!-- fa-user--><i class="fa fa-lg fa-fw"></i> <span class="menu-item-parent">Issue Tracking</span></a>
			</li>			
			  
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> 
		<i class="fa fa-arrow-circle-left hit"></i> 
	</span>

</aside>
<!-- END NAVIGATION -->