<?php //include('header.php');?>
<?php //include('inner-menu.php');?>
<?php //include('left-menu.php');?>

	<aside id="left-panel">
	<!-- User info -->
	<div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 			
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
			<!--<i class="fa fa-lg fa-fw fa-user"></i>
				<img src="img/avatars/sunny.png" alt="me" class="online" /> -->
				<span></span> 
			</a> 			
		</span>
	</div>
	<!-- end user info -->

	<!-- NAVIGATION : This navigation is also responsive-->
	<nav>
		<ul>
			<li>
				<a href="<?php echo base_url();?>device" title="Device List"><!-- fa-home --><i class="fa fa-lg fa-fw "></i> <span class="menu-item-parent">Device List</span></a>
			</li>
			<li class="active">
				<a href="<?php echo base_url();?>issue" title="Issue Tracking"><!-- fa-user--><i class="fa fa-lg fa-fw"></i> <span class="menu-item-parent">Issue Tracking</span></a>
			</li>			
			  
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> 
		<i class="fa fa-arrow-circle-left hit"></i> 
	</span>

</aside>
<!-- END NAVIGATION -->
		<!-- MAIN PANEL -->
		<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Issues Tracking </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<!-- <span class="widget-icon"> <i class="fa fa-table"></i> </span> -->
									<h2>Issues Tracking</h2>				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										<button type="button" class="btn btn-default add"  data-toggle="modal" data-target="#myModal">Add Issues</button>					
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">Issue ID</th>
													<th data-hide="phone">Raised Date</th>
													<th>Clarification</th>
													<th data-hide="phone">Device Name</th>
													<th data-hide="phone,tablet">Country</th>
													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><a  data-toggle="modal" data-target="#myModal2">Dell</a></td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
												<tr>
													<td>Dell</td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
												</tr>
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
				<!-- modal-->
				 <div class="modal fade" id="myModal2" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Device Details</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-sm-4" for="tags">Issue ID</label>
											<span class="help-inline">1456</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Reported Date</label>
											<span class="help-inline">07-01-2016</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Country</label>
											<span class="help-inline">UK</span>
										</div>										
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device Name</label>
											<span class="help-inline">Dell</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Issue Description</label>
											<span class="help-inline">Hardware failure reported at 3 PM.</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Added On</label>
											<span class="help-inline">04-01-2016</span>
										</div>
									</div>	
								</div>								
							</div>
							<div class="modal-footer">
								<div class="row">
									<div class="col-md-12 text-center">
										<button type="button" class="btn btn-default" >
											Edit
										</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">
											cancel
										</button>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	
					<!-- modal-->
				 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Add Issue</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<form class="form-horizontal" action="">
										<div class="col-md-12">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Issue ID</label>
												<div class="col-sm-6"><strong class="line-height32">1456</strong>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Reported Date</label>
												<div class="col-sm-6">
													<strong class="line-height32">07-01-2016</strong>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Country</label>
												<div class="col-sm-6">
													<select class="form-control">
														<option class="selected">Select Country</option>
														<option>UK</option>
														<option>AT</option>
														<option>PT</option>
														<option>SE</option>
													</select>
												</div>
											</div>									
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device Name</label>
												<div class="col-sm-6">
													<select class="form-control">
														<option class="selected">Select Device</option>
														<option>Dell</option>
														<option>HP</option>
														<option>Sony</option>
														<option>Acer</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Issue Description</label>
												<div class="col-sm-6">
													<textarea class="form-control" placeholder="Description"></textarea>
												</div>
											</div>
										</div>	
									</form>
								</div>
							</div>
							<div class="modal-footer">
								<div class="row">
									<div class="col-md-12 text-center">
										<button type="button" class="btn btn-default" >
											Save
										</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">
											cancel
										</button>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	
<!-- modal-->
				 <div class="modal fade" id="myModal3" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Conformation</h4>
							</div>
							<div class="modal-body dsp-block" >
								<div class="col-sm-12">
									<div class="row">			
										<strong class="text-center">Issue logged in successfully - Issue no 1456</strong>
									</div>
								</div>
							</div>
							<div class="modal-footer text-center">
								<button type="button" class="btn btn-default" >
									OK
								</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									CANCEL
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		

						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
<?php //include('footer.php');?>

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    var spinner = $( ".spinner" ).spinner();
    $( "button" ).button();
  });
  </script>