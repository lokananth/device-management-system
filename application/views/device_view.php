<?php //include('header.php');?>
<?php //include('inner-menu.php');?>
<?php //include('left-menu.php');?>

<aside id="left-panel">
	<!-- User info -->
	<div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 			
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
			<!--<i class="fa fa-lg fa-fw fa-user"></i>
				<img src="img/avatars/sunny.png" alt="me" class="online" /> -->
				<span></span> 
			</a> 			
		</span>
	</div>
	<!-- end user info -->

	<!-- NAVIGATION : This navigation is also responsive-->
	<nav>
		<ul>
			<li class="active">
				<a href="<?php echo base_url();?>device" title="Device List"><!-- fa-home --><i class="fa fa-lg fa-fw "></i> <span class="menu-item-parent">Device List</span></a>
			</li>
			<li>
				<a href="<?php echo base_url();?>issue" title="Issue Tracking"><!-- fa-user--><i class="fa fa-lg fa-fw"></i> <span class="menu-item-parent">Issue Tracking</span></a>
			</li>			
			  
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> 
		<i class="fa fa-arrow-circle-left hit"></i> 
	</span>

</aside>
<!-- END NAVIGATION -->
		<!-- MAIN PANEL -->
		<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Device List </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<!-- <span class="widget-icon"> <i class="fa fa-table"></i> </span> -->
									<h2>Device List </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="col-sm-12 col-md-12 prod-form">
											<div class="row">
												<div class="col-sm-4 col-md-4">
													<div class="row">
														<div class="col-sm-5">
															<strong>Total Device<span class="pull-right">:</span></strong>
														</div>
														<div class="col-sm-7">
															1000
														</div>
													</div>
												</div>
												<div class="col-sm-4 col-md-4">
													<div class="row">
														<div class="col-sm-5">
															<strong>In Use<span class="pull-right">:</span></strong>
														</div>
														<div class="col-sm-7">
															1000
														</div>		
													</div>
												</div>
												<div class="col-sm-4 col-md-4">
													<div class="row">
														<div class="col-sm-5">
															<strong>Not In Use<span class="pull-right">:</span></strong>
														</div>
														<div class="col-sm-7">
															1000
														</div>	
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-12 col-md-12 prod-form">
											<div class="row">
												<form name="selectDeviceform" class="form-horizontal" id="selectDeviceform" method="post" action="<?php echo base_url();?>device/deviceSearch">
													<div class="col-sm-4 col-md-4">
														<div class="form-group">
															<label class="control-label col-sm-5 text-left">Device Status<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-7">
																<select class="form-control" id="deviceStatus" name="deviceStatus">
																	<option value="">Select</option>
																	<option value="1" <?php if(isset($varDeviceStatus) && $varDeviceStatus!='' && $varDeviceStatus=="1"){ ?> selected <?php } ?>>In Use</option>
																	<option value="0" <?php if(isset($varDeviceStatus) && $varDeviceStatus!='' && $varDeviceStatus=="0"){ ?> selected <?php } ?>>Not In Use</option>
																</select>
															</div>	
														</div>														
													</div>
													<div class="col-sm-4 col-md-4">
														<div class="form-group">
															<label class="control-label col-sm-5 text-left">Country<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-7">
																<select class="form-control" id="country" name="country">
																	<option value="">Select Country</option>
																	<?php  foreach($arrCountryList as $key=>$value){ ?>
																	<option  <?php if(isset($varCountry) && $varCountry!='' && $varCountry==$value['countryname']){ ?> selected <?php } ?> value="<?php echo $value['countrycode'].'~'.$value['countryname']; ?>"><?php echo $value['countryname']; ?></option>      
																	<?php } ?>
																</select>
															</div>	
														</div>														
													</div>
													<div class="col-sm-4 col-md-4">
														<div class="form-group">
															<div class="col-sm-12">
																<input type="submit" name="selectDeviceList" id="selectDeviceList" value="GO" class="btn btn-primary">
																<!--button type="submit" class="btn btn-primary">GO</button-->
															</div>	
														</div>														
													</div>
												</form>
											</div>
										</div>
										<div class="col-sm-12">
											<button type="button" class="btn btn-default add"  id="addDevice" data-toggle="modal" data-target="#myModal">Add Device</button>
											<!-- <button type="button" class="btn btn-default add"  data-toggle="modal" data-target="#myModal2">Update Products</button>	 -->
										</div>
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-hide="phone">Device Name</th>
													<th data-class="expand">Added Date</th>
													<th>Device Type</th>
													<th data-hide="phone">Status</th>
													<th data-hide="phone,tablet">Country</th>
													<th data-hide="phone,tablet">Action</th>
													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal2">Dell</a></td>
													<td>05-01-2016</td>
													<td>Storage unit</td>
													<td>In use</td>
													<td>UK</td>
													<td><a href="#"  data-toggle="modal" data-target="#myModal3"><i class="fa fa-bug"></i>&nbsp;Retire</a></td>
												</tr>																										
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
				<!-- modal-->
				 <div class="modal fade" id="myModal2" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Device Details</h4>
							</div>
							<div class="modal-body">
								<div class="row">
								  <form name="editDeviceform" class="form-horizontal" id="editDeviceform" method="post" action="<?php echo base_url();?>device/editDeviceInformation">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-sm-4" for="tags">Country</label>
											<select class="form-control" id="editDeviceCountry" name="editDeviceCountry" autocomplete="off">
												<option value="">Select Country</option>
												<?php  foreach($arrCountryList as $key=>$value){ ?>
												<option  value="<?php echo $value['countrycode'].'~'.$value['countryname']; ?>"><?php echo $value['countryname']; ?></option>      
												<?php } ?>
											</select>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device Name</label>
											<input type="text" class="form-control" value=""  placeholder="Device Name" id="editDeviceName" name="editDeviceName"  maxlength="40" autocomplete="off" />
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device ID</label>
											<input type="text" class="form-control" value="" placeholder="Device ID" id="editDeviceSerialNo" name="editDeviceSerialNo"  maxlength="40" autocomplete="off" />
										</div>										
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device Type</label>
											<input type="text" class="form-control" value=""  placeholder="Device Type" id="editDeviceType" name="editDeviceType"  maxlength="40" autocomplete="off" />
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device Description</label>
											<textarea class="form-control" placeholder="Description" id="editDeviceDescription" name="editDeviceDescription"  maxlength="200" autocomplete="off"></textarea>
										</div>
										<!--div class="form-group">
											<label class="col-sm-4" for="tags">Added On</label>
											<span class="help-inline">04-01-2016</span>
										</div-->
									</div>	
								</div>
								<div class="row">
									<div class="col-md-12 text-center">
										<!--button type="button" class="btn btn-default" >
											Edit
										</button-->
										<input type="submit" name="editDeviceInfo" id="editDeviceInfo" value="Edit" class="btn btn-default">
										<button type="button" class="btn btn-primary" data-dismiss="modal">
											cancel
										</button>
									</div>
								</div>
								</form>
								<div class="row m-t-20">
									<div class="col-md-12">
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th>Issue ID</th>
													<th>Raised Date</th>
													<th>Clarification</th>
													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1023</td>
													<td>05-01-2016</td>
													<td>device failure</td>
												</tr>
												<tr>
													<td>1023</td>
													<td>05-01-2016</td>
													<td>device failure</td>
												</tr>
												<tr>
													<td>1023</td>
													<td>05-01-2016</td>
													<td>device failure</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	
					<!-- modal-->
				 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Add Device</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<form name="addDeviceform" class="form-horizontal" id="addDeviceform" method="post" action="<?php echo base_url();?>device/addDeviceInformation">
										<div class="col-md-12">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Country</label>
												<div class="col-sm-6">
													<select class="form-control" id="addDeviceCountry" name="addDeviceCountry" autocomplete="off">
														<option value="">Select Country</option>
														<?php  foreach($arrCountryList as $key=>$value){ ?>
														<option  value="<?php echo $value['countrycode'].'~'.$value['countryname']; ?>"><?php echo $value['countryname']; ?></option>      
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device Name</label>
												<div class="col-sm-6">
													<input type="text" class="form-control" value=""  placeholder="Device Name" id="deviceName" name="deviceName"  maxlength="40" autocomplete="off" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device ID</label>
												<div class="col-sm-6">
													<input type="text" class="form-control" value="" placeholder="Device ID" id="deviceSerialNo" name="deviceSerialNo"  maxlength="40" autocomplete="off" />
												</div>
											</div>										
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device Type</label>
												<div class="col-sm-6">
													<input type="text" class="form-control" value=""  placeholder="Device Type" id="deviceType" name="deviceType"  maxlength="40" autocomplete="off" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device Description</label>
												<div class="col-sm-6">
													<textarea class="form-control" placeholder="Description" id="deviceDescription" name="deviceDescription"  maxlength="200" autocomplete="off"></textarea>
												</div>
											</div>
										</div>										
								</div>
								<div class="row">
									<div class="col-md-12 text-center">
										<!--button type="button" class="btn btn-default" >
											OK
										</button-->
										<input type="submit" name="addDeviceInfo" id="addDeviceInfo" value="OK" class="btn btn-default">
										<button type="button" class="btn btn-primary" data-dismiss="modal">
											cancel
										</button>
									</div>
								</div>
								</form>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	<!-- modal-->
				 <div class="modal fade" id="myModal3" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
							</div>
							<form name="addDeviceform" class="form-horizontal" id="addDeviceform" method="post" action="<?php echo base_url();?>device/retireDevice">
							<input type="hidden" name="deviceConfirm" id="deviceConfirm" value="yes" />
							<input type="hidden" name="deviceId" id="deviceId" value="" />
							<div class="modal-body dsp-block" >
								<div class="col-sm-12">
									<div class="row">			
										<strong class="text-center">Do  you want to mark this device as retired</strong>
									</div>
								</div>
							</div>
							<div class="modal-footer text-center">
								<!--button type="button" class="btn btn-default" >
									OK
								</button-->
								<input type="submit" name="retireDeviceInfo" id="retireDeviceInfo" value="OK" class="btn btn-default">
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									CANCEL
								</button>
							</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
	<!-- modal end-->	
	
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
<?php //include('footer.php');?>

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    var spinner = $( ".spinner" ).spinner();
    $( "button" ).button();
  });
  
  $(document).ready(function() {
	//alert('hai');
	//alert($('#country').val());
	// validate the comment form when it is submitted

		// validate signup form on keyup and submit
		/*$("#selectDeviceform").validate({			
				rules: {
					deviceStatus: "required",
					//country: "required",				
				},
				messages: {
					deviceStatus: "Please select a device status",
					//country: "Please select a country",							
				}
			
		});	*/
		
		
		/*$("#deviceStatus").blur(function(){
			$("#deviceStatus").valid();
		});	
		$("#country").blur(function(){
			$("#country").valid();
		});*/
		
		// add device form validation
		$("#addDeviceform").validate({			
				rules: {
					addDeviceCountry: "required",
					deviceName: "required",	
					deviceSerialNo: "required",
					deviceType: "required",
					deviceDescription: "required",
				},
				messages: {
					addDeviceCountry: "Please select a country",
					deviceName: "Please enter a device name",
					deviceSerialNo: "Please enter a serial number",
					deviceType: "Please enter a device type",
					deviceDescription: "Please enter a device description",
				}			
		});
		
		$("#addDeviceCountry").blur(function(){
			$("#addDeviceCountry").valid();
		});	
		$("#deviceName").blur(function(){
			$("#deviceName").valid();
		});
		$("#deviceSerialNo").blur(function(){
			$("#deviceSerialNo").valid();
		});
		$("#deviceType").blur(function(){
			$("#deviceType").valid();
		});
		$("#deviceDescription").blur(function(){
			$("#deviceDescription").valid();
		});
		
		
		$("#addDevice").click(function(){			
			$("em").empty();
		});
		
		//edit form validation
		$("#editDeviceform").validate({			
				rules: {
					editDeviceCountry: "required",
					editDeviceName: "required",	
					editDeviceSerialNo: "required",
					editDeviceType: "required",
					editDeviceDescription: "required",
				},
				messages: {
					editDeviceCountry: "Please select a country",
					editDeviceName: "Please enter a device name",
					editDeviceSerialNo: "Please enter a serial number",
					editDeviceType: "Please enter a device type",
					editDeviceDescription: "Please enter a device description",
				}			
		});
		
		$("#editDeviceCountry").blur(function(){
			$("#editDeviceCountry").valid();
		});	
		$("#editDeviceName").blur(function(){
			$("#editDeviceName").valid();
		});
		$("#editDeviceSerialNo").blur(function(){
			$("#editDeviceSerialNo").valid();
		});
		$("#editDeviceType").blur(function(){
			$("#editDeviceType").valid();
		});
		$("#editDeviceDescription").blur(function(){
			$("#editDeviceDescription").valid();
		});
		
		
		$("#addDevice").click(function(){			
			$("em").empty();
		});
		
  });
  </script>