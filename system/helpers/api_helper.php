<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright		Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @copyright		Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/array_helper.html
 */

// ------------------------------------------------------------------------
if ( ! function_exists('ApiPostHeader'))
{
	 function ApiPostHeader($apiType, $apiValue) {
        //$apiURL = $this->_apiurl[$apiType];
        $apiURL = $apiType;
        //print_r(json_encode($apiValue));exit;
        //header("HTTP/1.1 200 OK");
        //header("Content-Type: application/json");
		$apiHeader = array(        	
	        'Authorization: mundiovectone z0ejtXAoY2BpLuwqu6KHYjHREK6ll60vFNt81YM5sx8=',
			'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4',
			'Content-MD5: 917200022538',
			'User-Agent: mundiovectone',
			'Accept: application/json',
			'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiValue));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $apiHeader);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        //print_r($output);
        return json_decode($output, true);
    }
}

// ------------------------------------------------------------------------

/**
 * Random Element - Takes an array as input and returns a random element
 *
 * @access	public
 * @param	array
 * @return	mixed	depends on what the array contains
 */
if ( ! function_exists('ApiGetHeader'))
{
	 function ApiGetHeader($apiType, $apiParmValue, $apiHeader) {
        //$apiURL = $this->_apiurl[$apiType];
        $apiURL = $apiType;
       //  echo $apiURL . '?' . $apiParmValue;exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiURL . '?' . $apiParmValue);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $apiHeader);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
       //echo curl_errno($ch);
        return json_decode($output, true);
    }
}

	function sendMail($to,$subject,$message){
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers		
		$headers .= 'From: Roaming Activation System <noreply@vectone.com>' . "\r\n";
		$headers .= 'To: ' . "\r\n";
		$headers .= 'Cc: ' . "\r\n";
		$headers .= 'Bcc: ' . "\r\n";

		// Mail it
		mail($to, $subject, $message, $headers);
		return true;
	}

// --------------------------------------------------------------------



/* End of file api_helper.php */
/* Location: ./system/helpers/api_helper.php */